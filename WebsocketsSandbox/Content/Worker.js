﻿var ports = [];      //objects containing port, tab id and a flag indicating ping replied
var pingsSent = 0;   //how many pings have we sent so far
var pingsToSend = 3; //after sending that many pings, ports not having replied to any are removed
var nextTabId = 0;
var pingInterval = 2000; //it takes at most ((pingsToSend+1) * pingInterval) ms to detect a closed tab

var useShared = constructor.toString().indexOf("SharedWorker") !== -1;
var post;

if (useShared) {
    onconnect = function(e) {
        var port = e.ports[0];

        port.addEventListener('message',
            function(e) {
                if (e.data.cmd === "pong") {
                    var thePorts = ports.filter(function(p) { return p.tabId === e.data.tabId });
                    if (thePorts.length === 0) {
                        post("*** WTF??? IDK that tab: " + e.data.tabId);
                    } else {
                        thePorts[0].pingReplied = true;
                        thePorts[0].port.postMessage({ evt: "text", content: ">>> Got your pong!" });
                    }
                }
            });

        ports.push({
            port: port,
            tabId: nextTabId,
            pingReplied: true
        });
        port.start();

        port.postMessage({ evt: "tabId", content: nextTabId });
        post("--- New tab, id = " + nextTabId);
        nextTabId++;

        post(ports.length, "tabscount");
    }

    setInterval(function() {
        if (pingsSent === pingsToSend) {
            var deadPorts = ports.filter(function(p) { return !p.pingReplied });
            ports = ports.filter(function(p) { return p.pingReplied });
            deadPorts.forEach(function(p) {
                post("!!! lost tab: " + p.tabId);
            });
            ports.forEach(function(p) {
                p.pingReplied = false;
            });
            pingsSent = 0;
            post(ports.length, "tabscount");
        } else {
            ports.forEach(function(p) {
                if (!p.pingReplied) {
                    p.port.postMessage({ evt: "ping" });
                    post("--- ping sent to: " + p.tabId);
                }
            });
            pingsSent++;
        }
    },
    pingInterval);  

    post = function(message, evt) {
        ports.forEach(function(item) {
            item.port.postMessage({ evt: evt || "text", content: message });
        });
    }
} else {
    post = function (message, evt) {
        postMessage({ evt: evt || "text", content: message });
    }
}

ws = new WebSocket("ws://localhost:6002");

ws.onopen = function () {
    post("--- Connected to Websocket");
};

ws.onerror = function () {
    post("*** WEBSOCKET ERROR");
};

ws.onmessage = function (e) {
    post(e.data, "ws-data");
};

