﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using StackExchange.NetGain.WebSockets;

namespace WebsocketsSandbox
{
    public class SocketProcessor : WebSocketsMessageProcessor
    {
        int index;
        private Timer timer;
        readonly List<WebSocketConnection> connections = new List<WebSocketConnection>();

        public SocketProcessor()
        {
            timer = new Timer(OnTimer, null, (int)100, 1000);
        }

        private void OnTimer(object state)
        {
            foreach(var connection in connections) 
            {
                Send(connection, index.ToString());
                index++;
            }
        }

        private void SendToAll(string message)
        {
            foreach(var connection in connections) 
            {
                Send(connection, message);
            }
        }

        protected override void OnOpened(WebSocketConnection connection)
        {
            Debug.WriteLine("Connected: " + connection.Host);
            connections.Add(connection);
            SendToAll($"--- Connected: {connection.Host}, {connections.Count} connections");
            base.OnOpened(connection);
        }

        protected override void OnClosed(WebSocketConnection connection)
        {
            Debug.WriteLine("Closed: " + connection.Host);
            connections.Remove(connection);
            SendToAll($"--- Disconnected: {connection.Host}, {connections.Count} connections");
            base.OnClosed(connection);
        }
    }
}