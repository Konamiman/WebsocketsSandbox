﻿using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using StackExchange.NetGain;
using StackExchange.NetGain.WebSockets;

namespace WebsocketsSandbox
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private TcpServer tcpserver;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            IPEndPoint endpoint = new IPEndPoint(IPAddress.Loopback, 6002);
            tcpserver = new TcpServer();
            tcpserver.ProtocolFactory = WebSocketsSelectorProcessor.Default;
            tcpserver.ConnectionTimeoutSeconds = 60;
            tcpserver.MessageProcessor = new SocketProcessor();
            tcpserver.Start("abc", endpoint);
        }
    }
}
