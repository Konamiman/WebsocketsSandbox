﻿## Web workers + Web sockets example ##

Example of a web socket that is accesed from a web worker. Shared web workers are used if available, and in that case they are aware of the existing tabs (a polling mechanism is used for that). The important files are `Views/Home/Index.cshtml` and `Content/Worker.js`.